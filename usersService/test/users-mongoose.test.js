const usersModel = require('../dist/models/users-mongoose');

test(`test to create user`, async function() {
    const user = await usersModel.createUser({
        firstName: 'Mehmet Egemen',
        familyName: 'Albayrak',
        email: 'mehmetegemenalbayrak@gmail.com',
        birthDate: new Date('04/23/1944'),
        password: '1234'
    });

    expect(user.firstName).toBe('Mehmet Egemen');
    expect(user.familyName).toBe('Albayrak');
    expect(user.email).toBe('mehmetegemenalbayrak@gmail.com');
    expect(user.birthDate).toEqual(new Date('04/23/1944'));
    expect(user.password.length).toBeGreaterThan(4);
});

test(`test to update user's social login`, async function(){
    const user = await usersModel.updateUser(
        {
            provider: 'facebook',
            accessToken: 'access-token',
            email: 'mehmetegemenalbayrak@gmail.com'
        }
    );

    expect(user.provider).toBe('facebook');
    expect(user.accessToken).toBe('access-token');
});

test(`test to update users remaining data`, async function(){
    const user = await usersModel.updateUser(
        {
            firstName: 'Umut Alp',
            familyName: 'Albayrak',
            email: 'mehmetegemenalbayrak@gmail.com'
        }
    );

    expect(user.firstName).toBe('Umut Alp');
    expect(user.familyName).toBe('Albayrak');
});

test(`test to find user`, async function(){
    const user = await usersModel.findUserByEmail(
        {
            email: 'mehmetegemenalbayrak@gmail.com'
        }
    );
    
    expect(user.firstName).toBe('Umut Alp');
    expect(user.familyName).toBe('Albayrak');
});

test(`test to check password`, async function(){
    const check = await usersModel.passwordCheck(
        {
            email: 'mehmetegemenalbayrak@gmail.com',
            password: '1234'
        }
    )

    expect(check.check).toBe(true);
});

test(`test to destroy user`, async function(){
    await usersModel.destroyUser(
        {
            email: 'mehmetegemenalbayrak@gmail.com'
        }
    );
    const user = await usersModel.findUserByEmail(
        {
            email: 'mehmetegemenalbayrak@gmail.com'
        }
    );

    expect(user).toBeNull();
});