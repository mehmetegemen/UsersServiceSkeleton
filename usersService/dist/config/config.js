"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = {
    'USERS_MONGO_URL': process.env.USERS_MONGO_URL,
    'PORT': process.env.PORT || 3003
};
