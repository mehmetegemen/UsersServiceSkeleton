"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = __importDefault(require("util"));
const restify_1 = __importDefault(require("restify"));
const usersModel = __importStar(require("./models/users-mongoose"));
const debug_1 = __importDefault(require("debug"));
const config_1 = __importDefault(require("./config/config"));
const error = debug_1.default('users:error');
const log = debug_1.default('users:server');
let server = restify_1.default.createServer({
    name: 'Users-Service',
    version: '0.1'
});
server.use(restify_1.default.plugins.authorizationParser());
server.use(check);
server.use(restify_1.default.plugins.queryParser());
server.use(restify_1.default.plugins.bodyParser({
    mapParams: true
}));
server.post('/create-user', function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const doc = yield usersModel.createUser(req.body);
            res.send(doc);
            next(false);
        }
        catch (err) {
            error(`User could not be created: ${err.stack}`);
        }
    });
});
server.post('/update-user', function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const doc = yield usersModel.updateUser(req.body);
            res.send(doc);
            next(false);
        }
        catch (err) {
            error(`User could not be updated: ${err.stack}`);
        }
    });
});
server.get('/find-user-by-email/:email', function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let doc = yield usersModel.findUserByEmail({ email: req.params.email });
            // Check if document is found
            if (doc === null) {
                // Not found
                // Exit with failure
                next(false);
                return res.send({
                    success: false,
                    message: 'User not found.'
                });
            }
            res.send(usersModel.sanitize(doc));
            next(false);
        }
        catch (err) {
            error(`User could not be found: ${err.stack}`);
        }
    });
});
server.get('/find-user-by-id/:_id', function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            let doc = yield usersModel.findUserById({ _id: req.params._id });
            // Check if document is found
            if (doc === null) {
                // Not found
                // Exit with failure
                next(false);
                return res.send({
                    success: false,
                    message: 'User not found.'
                });
            }
            res.send(usersModel.sanitize(doc));
            next(false);
        }
        catch (err) {
            error(`User could not be found: ${err.stack}`);
        }
    });
});
server.get('/destroy-user/:email', function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield usersModel.destroyUser(req.params.email);
            res.send({
                success: true,
                message: 'User is destroyed'
            });
            next(false);
        }
        catch (err) {
            error(`User could not be destroyed: ${err.stack}`);
        }
    });
});
server.post('/password-check', function (req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const check = yield usersModel.passwordCheck(req.body);
            res.send(check);
            next(false);
        }
        catch (err) {
            error(`password-check error: ${err.stack}`);
        }
    });
});
server.listen(config_1.default.PORT, "localhost", function () {
    log(server.name + ' listening at ' + server.url);
});
// Mimic API Key authentication.
var apiKeys = [{
        user: 'them',
        key: '76ba94ee-51f2-4405-a894-50123a83f441'
    }];
function check(req, res, next) {
    if (req.authorization) {
        let found = false;
        for (let auth of apiKeys) {
            if (auth.key === req.authorization.basic.password
                && auth.user === req.authorization.basic.username) {
                found = true;
                break;
            }
        }
        if (found)
            next();
        else {
            res.send(401, new Error("Not authenticated"));
            error('Failed authentication check '
                + util_1.default.inspect(req.authorization));
            next(false);
        }
    }
    else {
        res.send(500, new Error('No Authorization Key'));
        error('NO AUTHORIZATION');
        next(false);
    }
}
