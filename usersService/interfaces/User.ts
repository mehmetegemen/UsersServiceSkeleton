export interface IUser {
    firstName: string,
    familyName: string,
    birthDate: Date,
    email: string,
    password: string,
    provider?: string,
    accessToken?: string
}

export interface IUserObject {
    firstName?: string,
    familyName?: string,
    birthDate?: Date,
    email?: string,
    password?: string,
    provider?: string,
    accessToken?: string,
    [key: string]: string | undefined | Date
}