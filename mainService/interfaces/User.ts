export interface IUser {
    userId?: string,
    firstName?: string,
    familyName?: string,
    birthDate?: Date,
    email?: string,
    password?: string,
    provider?: string,
    accessToken?: string
}