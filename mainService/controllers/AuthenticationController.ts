import * as usersRest from '../models/users-rest'
import jwt from 'jsonwebtoken'
import config from '../config/config'
import debug from 'debug'
import { Request, Response } from 'express';
import { IUserModel } from '../schemas/User'
const error = debug('main:error');

function jwtSignUser (user: IUserModel) {
    const ONE_YEAR = 60 * 60 * 24 * 365;
    return jwt.sign(user, config.SECRET, {
        expiresIn: ONE_YEAR
    });
}

export default {
    async login (req: Request, res: Response) {
        try {
            if (!req.body.email || !req.body.password) {
                return res.send(
                    {
                        success: false,
                        message: 'Missing credentials.'
                    }
                );
            }
            // Find user in database
            const user: any = await usersRest.find(req.body);
            if (user.success === false) {
                // User is not found
                // Exit with failure
                return res.status(403).send(
                    {
                        success: false,
                        message: 'Login information is incorrect.'
                    }
                );
            }

            // User is found
            // Check if password is valid
            const isPasswordValid = (<any>await usersRest.passwordCheck(req.body)).check;
            if (!isPasswordValid) {
                // Password is invalid
                // Exit with failure
                return res.status(403).send(
                    {
                        success: false,
                        message: 'Login information is incorrect.'
                    }
                )
            }

            // Send User object and Token
            res.send(
                {
                    user,
                    token: jwtSignUser(<IUserModel>user)
                }
            )
        } catch (err) {
            error(`Login controller error: ${err.stack}`)
            res.status(500).send({
                success: false,
                message: 'Login error on server'
            });
        }
    }
}