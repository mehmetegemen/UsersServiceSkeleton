import expressRateLimit from 'express-rate-limit'

export const loginLimit = new expressRateLimit({
    // Limit request amount of logins
    windowMs: 1000 * 60, // 1 Minute
    max: 20 // Maximum 20 logins every minute 
});

export const registerLimit = new expressRateLimit({
    // Limit request amount of registers
    windowMs: 1000 * 60 * 60 * 2, // 2 hours
    max: 5 // Maximum 5 registers every 2 hour 
});