import { Application } from "express";
import AuthenticationController from './controllers/AuthenticationController'
import UserController from './controllers/UserController'

import isAuthenticated from './policies/isAuthenticated'
import UserControllerPolicy from './policies/UserControllerPolicy'
import AuthenticationControllerPolicy from './policies/AuthenticationControllerPolicy'

import * as rateLimits from './policies/rateLimits'

export default (app: Application) => {
    // Authentication Controllers
    app.post('/login', rateLimits.loginLimit, AuthenticationControllerPolicy.login, AuthenticationController.login); // tested
    
    // User Controllers
    app.post('/register', rateLimits.registerLimit, UserControllerPolicy.register,UserController.register); // tested
}