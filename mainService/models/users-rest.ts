import restify, { IResolvedJsonClient } from 'restify-clients'
import debug from 'debug'
import config from '../config/config'
import { IUser } from '../interfaces/User';
import { IUserModel } from '../schemas/User'

const log = debug('main:users-rest');
const error = debug('main:error');

const connectRest = function(){
    return new Promise((resolve, reject) => {
        try {
            resolve(restify.createJsonClient({
                url: <string>config.USERS_SERVICE_URL,
                version: '*'
            }));
        } catch (err) {
            error(`restify client connect error: ${err.stack}`);
        }
    }).then((client: any) => {
        // Server requires API Key
        // Login to server
        client.basicAuth('them', '76ba94ee-51f2-4405-a894-50123a83f441');
        return client;
    })
};

export async function create (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.post('/create-user', {
                firstName: data.firstName,
                familyName: data.familyName,
                birthDate: data.birthDate,
                email: data.email,
                password: data.password,
                provider: data.provider,
                accessToken: data.accessToken
            }, (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
}

export async function update (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.post('/update-user', {
                firstName: data.firstName,
                familyName: data.familyName,
                birthDate: data.birthDate,
                email: data.email,
                password: data.password,
                provider: data.provider,
                accessToken: data.accessToken
            }, (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function find (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.get('/find-user-by-email/' + data.email,
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function findById (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.get('/find-user-by-id/' + data.userId,
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function destroy (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.get('/destroy-user/' + data.email, 
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};

export async function passwordCheck (data: IUser) {
    try {
        const client = await connectRest();
        return new Promise((resolve, reject) => {
            client.post('/password-check', 
            {
                email: data.email,
                password: data.password
            },
            (err: any, req: Request, res: Response, obj: IUserModel) => {
                if (err) return reject(err);
                resolve(obj);
            });
        });
    } catch (err) {
        error(`Rest create error: ${err.stack}`);
    }
};