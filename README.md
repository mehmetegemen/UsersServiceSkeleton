# Users Service Skeleton
Project contains a users service and a main service. Users service has database operations of users and should serve isolated from outer net and only be exposed to main service. Main service contains restify client to connect to that internal API. I believe main service's policies will be very beneficial for you. I didn't implement social login and email authentication yet, I can happily accept a pull request for them. This is a TypeScript project, it is a good example for beginners to taste every bit of it.

In general, this small project will help you to bootstrap a project quickly without spending time on users service. Also it is a good project to learn something because it is small. I started to programming years ago but writing node.js for few months. I tried to learn from everyone along the way and wanted to pass it to beginners so they can have a good headstart.

You should change mongo db path environment variable in package.json and then start the service with `npm run start-dev`.

# License
MIT
